using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowResultsAuto : MonoBehaviour
{
    private int isDead = 0;
    public Text textResult;

    public void Awake()
    {
        isDead = 0;
    }

    // Update is called once per frame
    void Update()
    {
        isDead = PlayerPrefs.GetInt("Dead1");
        ShowResult();
    }

    void ShowResult()
    {
        if (isDead == 0)
        {
            textResult.text = "Defeat!";
        }
        else
        {
            textResult.text = "Victory!";
        }
    }
}