using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeAuto : MonoBehaviour
{
    private int isDead = 0;

    public void Awake()
    {
        isDead = 0;
    }
    // Update is called once per frame
    void Update()
    {
        isDead = PlayerPrefs.GetInt("Dead1");
        if (isDead == 1)
        {
            StartCoroutine(WaitScene());
        }
    }
    IEnumerator WaitScene()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(4);

    }
}
