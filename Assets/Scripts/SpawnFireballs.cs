using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFireballs : MonoBehaviour
{
    public GameObject fireball;
    private float _minRandom, _maxRandom;
    private float nextActionTime = 0.0f;
    public float period = 0.4f;

  
    void Start()
    {
        _minRandom = transform.position.x - 10f;
        _maxRandom = transform.position.x + 10f;
    }

    
    void Update()
    {
        nextActionTime += Time.deltaTime;
        if (nextActionTime > period)
        {
            nextActionTime = 0.0f;
            Spawn();
        }
    }
    void Spawn()
    {

        Instantiate(fireball, new Vector3(Random.Range(_minRandom,_maxRandom),transform.position.y,0),Quaternion.identity);
    }
}
