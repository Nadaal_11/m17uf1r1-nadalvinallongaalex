using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowName : MonoBehaviour
{
    public Text TextNom;
    public new string name;

    // Start is called before the first frame update
    void Start()
    {
        name = PlayerPrefs.GetString("Nom");
        TextNom.text = "Player name: " + name;
    }
}
