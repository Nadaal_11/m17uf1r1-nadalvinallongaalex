using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    private float liveTime = 1f;
    public void OnExplosionAnimationFinished()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
       // if (other.CompareTag("Player")) GameManager.Instance.ReloadScene();
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Debug.Log("Xocat");
            GetComponent<Animator>().SetTrigger("Explosion");
            var rb = GetComponent<Rigidbody2D>();
            rb.gravityScale = 0f;
            rb.velocity = new Vector2(0, 0);
        }
        if (other.tag == "Player")
        {
            Debug.Log("Xocat");
            GetComponent<Animator>().SetTrigger("Explosion");
            Destroy(other.gameObject, liveTime);
        }
    }
}
