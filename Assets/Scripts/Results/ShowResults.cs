using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowResults : MonoBehaviour
{
    private int isDead;
    public Text textResult;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isDead = PlayerPrefs.GetInt("Dead");
        ShowResult();
    }

    void ShowResult()
    {
        if (isDead == 0)
        {
            textResult.text = "Victory!";
        }
        else
        {
            textResult.text = "Defeat!";
        }
    }
}
