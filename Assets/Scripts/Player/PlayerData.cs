using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerData : MonoBehaviour
{
    [SerializeField] private float _jumpForce;
    [SerializeField] private float speed;
    private bool isJumping;
    private float moveHorizontal;
    private bool isFacingRight = true;
    private int timerOff;
    private int isDead = 0;

    private Animator animator;
    private Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
       
    }

    // Update is called once per frame
    void Update()
    {
        moveHorizontal = Input.GetAxisRaw("Horizontal");
        ManualMovement();
        Jump();
        TimerFinish();
        timerOff = PlayerPrefs.GetInt("Timer");
        if (transform.position.y < -5f)
        {
            isDead = 1;
            SceneManager.LoadScene(2);
        }
        PlayerPrefs.SetInt("Dead", isDead);
    }
    
    void TimerFinish()
    {
        if  (timerOff == 2)
        {
            SceneManager.LoadScene(2);
        }
       
    }
    void ManualMovement()
    {
        _rb.velocity = new Vector2(moveHorizontal * speed, _rb.velocity.y);
        FlipManual();
        if (Mathf.Abs(_rb.velocity.x)<0.1f)
        {
            animator.SetBool("IsWalking", false);
            animator.SetBool("IsIdle", true);
        }
        else
        {
            animator.SetBool("IsWalking", true);
            animator.SetBool("IsIdle", false);
        }
    }
    private void FlipManual()
    {
        if (isFacingRight && moveHorizontal < 0f || !isFacingRight && moveHorizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }
    void Jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !isJumping) 
        { 
            _rb.AddForce(new Vector2(_rb.velocity.x, _jumpForce));
            animator.SetBool("IsJumping", true);
        } 
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = false;
            animator.SetBool("IsJumping", false);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isJumping = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "fireball")
        {
            animator.SetBool("IsDead", true);
            isDead = 1;
        }
    }

   




}
