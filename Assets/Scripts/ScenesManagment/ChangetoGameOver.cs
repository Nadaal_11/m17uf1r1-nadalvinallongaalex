using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangetoGameOver : MonoBehaviour
{
    private int isDead;

    // Update is called once per frame
    void Update()
    {
        isDead = PlayerPrefs.GetInt("Dead");
        if (isDead == 1)
        {

            StartCoroutine(WaitScene());
        }
    }
    IEnumerator WaitScene()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(2);

    }
}
