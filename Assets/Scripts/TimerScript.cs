using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour
{

    private float timerLeft = 60f;
    public bool timerOn = false;
    public int timerOn1 = 0;

    public Text TimerText;
    // Start is called before the first frame update
    void Start()
    {
        timerOn = true;
        timerOn1 = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerOn)
        {
            if (timerLeft > 0f)
            {
                timerLeft -= Time.deltaTime;
                updateTimer(timerLeft);
                timerOn1 = 1;
            }
            else
            {
                Debug.Log("Time is Up!");
                timerLeft = 0f;
                timerOn = false;
                timerOn1 = 2;
            }
        }
        PlayerPrefs.SetInt("Timer", timerOn1);
        timerOn1 = 0;
    }
    void updateTimer(float currentTime)
    {
        currentTime += 1f;
        float minutes = Mathf.FloorToInt(currentTime / 60);
        float secons = Mathf.FloorToInt(currentTime % 60);
        TimerText.text = string.Format("{0:00} : {1:00}", minutes, secons);
    }
}
