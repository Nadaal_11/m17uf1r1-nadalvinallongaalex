using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoPlayerMovement : MonoBehaviour
{
    private SpriteRenderer _sprite;
    private Animator _animator;
    private RaycastHit2D _hitGround;

    private bool _raycast = false;
    private float _speed = 0.01f;
    private int _direction = 1; //Value 1 --> To right || Value -1 --> To left
 

    void Start()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        Physics2D.queriesStartInColliders = false;
        MoveAuto();
        _hitGround = Physics2D.Raycast(this.transform.position, new Vector3(_direction, -1, 0));
      
        if (!_hitGround) _raycast = true;
        else if (_hitGround.collider.tag == "Ground1") _raycast = false;
    }

    private void MoveAuto()
    {
        if (_raycast)
        {
            _direction *= -1;
            _speed *= -1;
            _sprite.flipX = (_sprite.flipX) ? false : true;
            //Cambia de direccion el raycast!
        }

        transform.position += new Vector3(_speed, 0, 0);
        _animator.SetBool("IsWalking", true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "fireball")
        {
            _animator.SetBool("IsDead", true);
            SceneManager.LoadScene(4);
        }
    }
}
